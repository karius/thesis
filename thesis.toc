\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Preface}{1}%
\contentsline {chapter}{\numberline {2}Introduction}{3}%
\contentsline {section}{\numberline {2.1}Systematic fitting}{3}%
\contentsline {section}{\numberline {2.2}Protein complexes}{4}%
\contentsline {section}{\numberline {2.3}The notion of a protein structure}{4}%
\contentsline {section}{\numberline {2.4}Experimental methods of structure determination}{5}%
\contentsline {subsection}{\numberline {2.4.1}Crystallography}{5}%
\contentsline {subsubsection}{The experimental setup and principle of acquisition}{5}%
\contentsline {subsubsection}{The Crystallography workflow}{7}%
\contentsline {subsubsection}{Crystallographic density maps as modeling resource}{9}%
\contentsline {subsection}{\numberline {2.4.2}Single particle cryo-EM}{9}%
\contentsline {subsubsection}{The experimental setup and principle of acquisition}{9}%
\contentsline {subsubsection}{The single particle cryo-EM workflow}{11}%
\contentsline {subsubsection}{Single particle cryo-EM maps as modeling resource}{13}%
\contentsline {subsection}{\numberline {2.4.3}Electron cryotomography}{13}%
\contentsline {subsubsection}{The experimental setup and principle of acquisition}{14}%
\contentsline {subsubsection}{The cryo-ET workflow}{15}%
\contentsline {subsubsection}{Cryo-ET maps as modeling resource}{16}%
\contentsline {subsection}{\numberline {2.4.4}Concepts of resolution}{17}%
\contentsline {subsection}{\numberline {2.4.5}Crosslinks}{18}%
\contentsline {section}{\numberline {2.5}Structural integrative modeling}{20}%
\contentsline {subsection}{\numberline {2.5.1}Structural Integrative Modeling as implemented in Assembline}{21}%
\contentsline {subsection}{\numberline {2.5.2}System definition, data collection and data curation}{22}%
\contentsline {subsubsection}{System definition}{22}%
\contentsline {subsubsection}{Data curation}{28}%
\contentsline {subsubsection}{Data collection}{30}%
\contentsline {subsection}{\numberline {2.5.3}Model sampling}{30}%
\contentsline {subsection}{\numberline {2.5.4}Analysis of models}{35}%
\contentsline {subsection}{\numberline {2.5.5}Systematic Fitting}{37}%
\contentsline {subsubsection}{Spatial sampling}{38}%
\contentsline {subsubsection}{Scoring in general}{40}%
\contentsline {subsubsection}{The overlap score}{40}%
\contentsline {subsubsection}{The cross correlation score}{41}%
\contentsline {subsubsection}{The correlation-about-mean score}{42}%
\contentsline {subsubsection}{Masked scores}{44}%
\contentsline {subsubsection}{The envelope score}{45}%
\contentsline {subsubsection}{The chamfer distance score}{47}%
\contentsline {subsection}{\numberline {2.5.6}Other systematic fitting software options}{49}%
\contentsline {subsubsection}{The situs software library}{49}%
\contentsline {subsubsection}{The PowerFit software}{49}%
\contentsline {subsubsection}{The gEMfitter software}{50}%
\contentsline {subsubsection}{Areas of advancement}{50}%
\contentsline {section}{\numberline {2.6}The TFIIIC protein complex}{50}%
\contentsline {section}{\numberline {2.7}Objectives}{51}%
\contentsline {chapter}{\numberline {3}Computational methods}{53}%
\contentsline {section}{\numberline {3.1}Algorithmic methods}{53}%
\contentsline {subsection}{\numberline {3.1.1}Fourier accelerated fitting}{53}%
\contentsline {subsection}{\numberline {3.1.2}Filters}{54}%
\contentsline {section}{\numberline {3.2}Data Structures and Algorithms}{56}%
\contentsline {subsection}{\numberline {3.2.1}Particle sets}{56}%
\contentsline {subsubsection}{The PDB file format}{64}%
\contentsline {subsection}{\numberline {3.2.2}Densities}{64}%
\contentsline {subsubsection}{Representation in computer memory}{65}%
\contentsline {subsection}{\numberline {3.2.3}Molecular density simulation}{67}%
\contentsline {subsubsection}{The MRC/CCP4 file format}{68}%
\contentsline {subsection}{\numberline {3.2.4}The connected components labling algorithm}{68}%
\contentsline {subsubsection}{Basic description of the algorithm}{68}%
\contentsline {subsubsection}{The parallel version of the algorithm}{72}%
\contentsline {section}{\numberline {3.3}Parallel programming}{73}%
\contentsline {subsection}{\numberline {3.3.1}The memory hierarchy and the processing model}{74}%
\contentsline {subsection}{\numberline {3.3.2}Communication between host and device}{80}%
\contentsline {subsection}{\numberline {3.3.3}Texture memory}{81}%
\contentsline {subsection}{\numberline {3.3.4}Atomic operations}{84}%
\contentsline {subsection}{\numberline {3.3.5}Dynamic Parallelism}{86}%
\contentsline {subsection}{\numberline {3.3.6}Special operations}{87}%
\contentsline {chapter}{\numberline {4}Mathematical methods}{90}%
\contentsline {section}{\numberline {4.1}Convergence}{90}%
\contentsline {subsection}{\numberline {4.1.1}The notion of convergence}{90}%
\contentsline {subsection}{\numberline {4.1.2}The Cauchy convergence test}{91}%
\contentsline {subsection}{\numberline {4.1.3}The Geweke convergence test}{92}%
\contentsline {section}{\numberline {4.2}Vector spaces}{93}%
\contentsline {subsection}{\numberline {4.2.1}The three-dimensional real vector space}{95}%
\contentsline {subsection}{\numberline {4.2.2}Linear maps between general vector spaces}{100}%
\contentsline {subsection}{\numberline {4.2.3}Representations of rotations}{102}%
\contentsline {subsubsection}{The axis-angle representation}{102}%
\contentsline {subsubsection}{The Euler angle representation}{102}%
\contentsline {subsubsection}{The quaternion representation}{104}%
\contentsline {subsubsection}{Construction of rotation grids using quaternions}{105}%
\contentsline {subsection}{\numberline {4.2.4}Function vector spaces}{106}%
\contentsline {subsubsection}{Chebyshev polynomials as an example of polynomial approximation}{108}%
\contentsline {subsubsection}{Quadrature via Chebyshev polynomials}{112}%
\contentsline {subsection}{\numberline {4.2.5}Wigner D-functions as a basis}{114}%
\contentsline {subsubsection}{Introduction and definitions}{114}%
\contentsline {subsubsection}{Rotational Sampling}{117}%
\contentsline {subsubsection}{Quadrature on SO(3)}{118}%
\contentsline {subsubsection}{Visualizing rotational sampling nodes}{119}%
\contentsline {section}{\numberline {4.3}Information Theory}{120}%
\contentsline {subsubsection}{Other information theoretic metrics}{124}%
\contentsline {chapter}{\numberline {5}Results}{125}%
\contentsline {section}{\numberline {5.1}TFIIIC modeling}{125}%
\contentsline {section}{\numberline {5.2}Contributions to the \emph {Assembline} structural modeling pipeline}{129}%
\contentsline {subsection}{\numberline {5.2.1}Geweke convergence test implementation}{129}%
\contentsline {subsection}{\numberline {5.2.2}Bash analysis tools}{129}%
\contentsline {section}{\numberline {5.3}Description of the GPU implementation of the modeling classes}{132}%
\contentsline {subsubsection}{Memory management and data representation}{132}%
\contentsline {subsubsection}{Execution model and process management}{134}%
\contentsline {section}{\numberline {5.4}Density Sampling}{136}%
\contentsline {subsection}{\numberline {5.4.1}The procedural basis}{136}%
\contentsline {subsection}{\numberline {5.4.2}The procedure}{138}%
\contentsline {section}{\numberline {5.5}The transformational sampling scheme}{143}%
\contentsline {subsection}{\numberline {5.5.1}The translational grid}{144}%
\contentsline {subsection}{\numberline {5.5.2}The rotational grid}{145}%
\contentsline {subsection}{\numberline {5.5.3}The combined grid}{146}%
\contentsline {subsection}{\numberline {5.5.4}Sampling node memory layout}{147}%
\contentsline {section}{\numberline {5.6}Detection of local optimal scores}{149}%
\contentsline {subsection}{\numberline {5.6.1}General principle and implementation}{149}%
\contentsline {subsection}{\numberline {5.6.2}GPU implementation}{151}%
\contentsline {subsection}{\numberline {5.6.3}User interface and benchmark}{152}%
\contentsline {section}{\numberline {5.7}Parallel Implementation of scoring functions}{153}%
\contentsline {subsection}{\numberline {5.7.1}The masked correlation-about-mean score}{153}%
\contentsline {subsubsection}{User interface}{159}%
\contentsline {subsubsection}{Benchmarks}{160}%
\contentsline {subsection}{\numberline {5.7.2}The chamfer distance score}{162}%
\contentsline {subsubsection}{User interface}{165}%
\contentsline {subsubsection}{Benchmarks}{165}%
\contentsline {subsection}{\numberline {5.7.3}The envelope score}{165}%
\contentsline {section}{\numberline {5.8}Connected Components Labeling}{167}%
\contentsline {subsection}{\numberline {5.8.1}Structure of the GPU kernels}{168}%
\contentsline {subsection}{\numberline {5.8.2}User interface and convenience functions and tests}{170}%
\contentsline {section}{\numberline {5.9}The partial surface score}{172}%
\contentsline {subsection}{\numberline {5.9.1}Score definition}{173}%
\contentsline {subsection}{\numberline {5.9.2}Score implementation}{174}%
\contentsline {section}{\numberline {5.10}Particle Alignment and RMSD Calculation implementation}{177}%
\contentsline {subsection}{\numberline {5.10.1}RMSD calculation}{177}%
\contentsline {section}{\numberline {5.11}Reinterpretation of systematic fitting results}{186}%
\contentsline {subsection}{\numberline {5.11.1}Conceptual exposition}{186}%
\contentsline {subsection}{\numberline {5.11.2}Ambiguity in systematic fitting and scoring methods}{187}%
\contentsline {section}{\numberline {5.12}Entropy measurements of systematic fitting score populations using model densities}{189}%
\contentsline {subsection}{\numberline {5.12.1}Comparing different scoring methods}{190}%
\contentsline {subsection}{\numberline {5.12.2}Comparing different translational positions}{192}%
\contentsline {section}{\numberline {5.13}Entropy measurements of systematic fitting score populations using experimental data}{194}%
\contentsline {subsection}{\numberline {5.13.1}Definition of the masked correlation-about-mean score}{195}%
\contentsline {subsection}{\numberline {5.13.2}GPU implementation, user interface and benchmark}{196}%
\contentsline {subsection}{\numberline {5.13.3}Comparison of entropies of different scoring types}{199}%
\contentsline {subsection}{\numberline {5.13.4}Comparison of entropies to optimize parameters}{201}%
\contentsline {chapter}{\numberline {6}Discussion}{202}%
\contentsline {section}{\numberline {6.1}The TFIIIC complex modeling project}{202}%
\contentsline {section}{\numberline {6.2}Systematic Fitting and Scoring functions}{202}%
\contentsline {section}{\numberline {6.3}Demonstrated applications and further possibilities}{204}%
\contentsline {subsection}{\numberline {6.3.1}The optimal score}{204}%
\contentsline {section}{\numberline {6.4}Regular sampling and numerical quadrature}{205}%
\contentsline {subsection}{\numberline {6.4.1}Finding local minima and maxima}{206}%
\contentsline {subsection}{\numberline {6.4.2}Reconstructing scoring spaces}{207}%
\contentsline {section}{\numberline {6.5}Information theoretic perspective on systematic fitting and integrative modeling in general}{210}%
\contentsline {subsection}{\numberline {6.5.1}Scoring method assessment}{210}%
\contentsline {subsection}{\numberline {6.5.2}Higher order considerations}{211}%
\contentsline {section}{\numberline {6.6}Parallel computing and integrative structural modeling}{213}%
\contentsline {subsection}{\numberline {6.6.1}Inherent suitability of structural data and algorithms in structural biology for parallel computing}{213}%
\contentsline {subsection}{\numberline {6.6.2}Implementation and Applications}{214}%
\contentsline {section}{\numberline {6.7}Partial surface score}{215}%
\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {7}Appendix}{224}%
\contentsline {section}{\numberline {7.1}Zusammenfassung}{224}%
\contentsline {section}{\numberline {7.2}Synopsis}{226}%
\contentsline {section}{\numberline {7.3}List of publications involving the author}{227}%
